package sic.test;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

/**
 * Siehe Java Grundelemente, JUC2 01.04 Java02
 *
 * Class `Test0104Java02`, defined in its file `Test0104Java02.java`.
 *
 * Actually `Test0104Java02.java` exists as `sic/test/Test0104Java02.java` on your filesystem,
 * because class `Test0104Java02` is defined as a member of package `sic.test`.
 */
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class Test0104Java02 {
	/**
	 * Global variable
	 * - always exists (lifecycle)
	 * - try to avoid (bad style)
	 */
	public static int global_variable = 0;

	/**
	 * Member variable of an instance of type Test0104Java01.
	 * - Exists as long as its instance of type Test0104Java01.
	 */
	int member_of_test0104java01_instance_variable = 0;
}
